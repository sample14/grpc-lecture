package sample

import (
	"gRPC_Lecture/pb"
	"github.com/golang/protobuf/ptypes"
)

// KeyKeyboard returns a new sample keyboard
func NewKeyboard() *pb.Keyboard {
	return &pb.Keyboard{
		Layout:  randomKeyboardLayout(),
		Backlit: randomBool(),
	}
}

// NewCPU returns a new sample CPU
func NewCPU() *pb.CPU {
	branch := randomCPUBranch()
	name := randomCPUName(branch)

	numberOfCores := randomInt(2, 8)
	numberThreads := numberOfCores * 2

	minGhz := randomFloat64(2.0, 3.5)
	maxGhz := randomFloat64(minGhz, 5.0)

	return &pb.CPU{
		Brand:         branch,
		Name:          name,
		NumberCores:   uint32(numberOfCores),
		NumberThreads: uint32(numberThreads),
		MinGhz:        minGhz,
		MaxGhz:        maxGhz,
	}
}

// NewGPU returns a new sample GPU
func NewGPU() *pb.GPU {
	branch := randomGPUBranch()
	name := randomGPUName(branch)

	minGhz := randomFloat64(1.0, 1.5)
	maxGhz := randomFloat64(minGhz, 4.0)

	memory := &pb.Memory{
		Value: uint64(randomInt(2, 6)),
		Unit:  pb.Memory_GIGABYTE,
	}

	return &pb.GPU{
		Brand:  branch,
		Name:   name,
		MinGhz: minGhz,
		MaxGhz: maxGhz,
		Memory: memory,
	}
}

// NewRAM returns a new sample RAM
func NewRAM() *pb.Memory {
	return &pb.Memory{
		Value: uint64(randomInt(4, 16)),
		Unit:  pb.Memory_GIGABYTE,
	}
}

// NewSSD returns a new sample SSD storage
func NewSSD() *pb.Storage {
	return &pb.Storage{
		Driver: pb.Storage_SSD,
		Memory: &pb.Memory{
			Value: uint64(randomInt(128, 1024)),
			Unit:  pb.Memory_GIGABYTE,
		},
	}
}

// NewHDD returns a new sample HDD storage
func NewHDD() *pb.Storage {
	return &pb.Storage{
		Driver: pb.Storage_SSD,
		Memory: &pb.Memory{
			Value: uint64(randomInt(1, 6)),
			Unit:  pb.Memory_TERABYTE,
		},
	}
}

// NewScreen returns a new sample screen
func NewScreen() *pb.Screen {
	return &pb.Screen{
		SizeInch:   randomFloat32(13, 17),
		Resolution: randomResolution(),
		Panel:      randomScreenPanel(),
		Multitouch: randomBool(),
	}
}

// NewLaptop returns a new sample laptop
func NewLaptop() *pb.Laptop {
	brand := randomLaptopBrand()
	name := randomLaptopName(brand)

	return &pb.Laptop{
		Id:          randomID(),
		Brand:       brand,
		Name:        name,
		Cpu:         NewCPU(),
		Memory:      NewRAM(),
		Gpus:        []*pb.GPU{NewGPU(), NewGPU()},
		Storages:    []*pb.Storage{NewSSD(), NewHDD()},
		Screen:      NewScreen(),
		Keyboard:    NewKeyboard(),
		Weight:      &pb.Laptop_WeightKg{WeightKg: randomFloat64(1.0, 3.0)},
		PriceUsd:    randomFloat64(900, 1800),
		ReleaseYear: uint32(randomInt(2015, 2020)),
		UpdatedAt:   ptypes.TimestampNow(),
	}
}
