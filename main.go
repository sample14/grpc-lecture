package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"net/http"
)

var (
	log = logrus.New()
	//HOST = "127.0.0.1"
	//port = "3308"
	host = "172.17.0.4"
	port = "3306"
)

func init() {
	log.Formatter = new(logrus.JSONFormatter)
	log.Formatter = new(logrus.TextFormatter) // Default
	log.Level = logrus.DebugLevel
}

func main() {
	fmt.Println("Main function")

	// Server
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	// Router
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.GET("/go-docker", goWithDocker)
	e.Logger.Fatal(e.Start("0.0.0.0:8080"))
}

func goWithDocker(c echo.Context) error {
	db, err := gorm.Open("mysql", "root:root@("+host+":"+port+")/school?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Student{})

	// Create
	db.Create(&Student{
		Name:    "Anh",
		Class:   "1",
		Comment: "No",
	})

	// Read
	var student Student
	db.First(&student, 1)

	return c.JSON(http.StatusOK, student)
}

type Student struct {
	gorm.Model
	Name    string
	Class   string
	Comment string
}
