package sample

import (
	"gRPC_Lecture/pb"
	"github.com/google/uuid"
	"math/rand"
)

func randomBool() bool {
	return rand.Intn(2) == 1
}

func randomKeyboardLayout() pb.Keyboard_Layout {
	switch rand.Intn(3) {
	case 1:
		return pb.Keyboard_QWERTY
	case 2:
		return pb.Keyboard_QWERTZ
	default:
		return pb.Keyboard_AZERTY
	}
}

func randomCPUBranch() string {
	return randomStringFromSet("Intel", "AMD")
}

func randomCPUName(branch string) string {
	if branch == "Intel" {
		return randomStringFromSet("Core i9", "Core i7", "Core i5", "Core i3")
	}
	return randomStringFromSet("Ryzen 7", "Ryzen 5", "Ryzen 3")
}

func randomGPUBranch() string {
	return randomStringFromSet("NVIDIA", "AMD")
}

func randomGPUName(branch string) string {
	if branch == "NVIDIA" {
		return randomStringFromSet("GTX1080", "GTX1650", "GTX950M", "GT930")
	}
	return randomStringFromSet("RX 590", "RX 580", "RX 570-XT", "RX Vega-56")
}

func randomStringFromSet(a ...string) string {
	n := len(a)
	if n == 0 {
		return ""
	}
	return a[rand.Intn(n)]
}

func randomInt(min int, max int) int {
	return min + rand.Intn(max-min+1)
}

func randomFloat64(min float64, max float64) float64 {
	return min + rand.Float64()*(max-min)
}

func randomFloat32(min float32, max float32) float32 {
	return min + rand.Float32()*(max-min)
}

func randomScreenPanel() pb.Screen_Panel {
	randVal := rand.Intn(3)
	switch randVal {
	case 1:
		return pb.Screen_IPS
	case 2:
		return pb.Screen_OLED
	case 3:
		return pb.Screen_AMOLED
	default:
		return pb.Screen_TN
	}
}

func randomResolution() *pb.Screen_Resolution {
	width := randomInt(1080, 4320)
	height := randomInt(1080, 4320)

	return &pb.Screen_Resolution{
		Width:  uint32(width),
		Height: uint32(height),
	}
}

func randomID() string {
	return uuid.New().String()
}

func randomLaptopBrand() string {
	return randomStringFromSet("Apple", "Dell", "Lenovo")
}

func randomLaptopName(brand string) string {
	switch brand {
	case "Apple":
		return randomStringFromSet("Macbook Air", "Macbook Pro")
	case "Dell":
		return randomStringFromSet("Latitude", "Vostro", "XPS", "Inspiron", "Alienware")
	case "Lenovo":
		return randomStringFromSet("Thinkpad", "Yoga", "Legion")
	default:
		return "No brand"
	}
}
