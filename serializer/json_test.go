package serializer

import (
	"gRPC_Lecture/pb"
	"github.com/golang/protobuf/proto"
	"testing"
)

func TestProtobufToJSON(t *testing.T) {
	type args struct {
		message proto.Message
	}
	tests := []struct {
		name     string
		args     args
		wantJson string
		wantErr  bool
	}{
		{
			name: "1. Success",
			args: args{
				message: &pb.Keyboard{
					Layout:  0,
					Backlit: false,
				},
			},
			wantJson: "{\n    \"layout\": \"UNKNOWN\",\n    \"backlit\": false\n}",
			wantErr:  false,
		},
		{
			name: "2. Error",
			args: args{
				message: nil,
			},
			wantJson: "",
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotJson, err := ProtobufToJSON(tt.args.message)
			if (err != nil) != tt.wantErr {
				t.Errorf("ProtobufToJSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotJson != tt.wantJson {
				t.Errorf("ProtobufToJSON() gotJson = %v, want %v", gotJson, tt.wantJson)
			}
		})
	}
}
