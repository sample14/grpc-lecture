package serializer

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
)

// WriteProtobufToBinaryFile writes protocol buffer message to binary file
func WriteProtobufToBinaryFile(message proto.Message, filename string) error {
	data, err := proto.Marshal(message)
	if nil != err {
		return fmt.Errorf("cannot marshal proto message to binary: %w", err)
	}

	if err = ioutil.WriteFile(filename, data, 0644); nil != err {
		return fmt.Errorf("cannot write binary data to file: %w", err)
	}

	return nil
}

// ReadProtobufFromBinaryFile reads protocol buffer message from binary file
func ReadProtobufFromBinaryFile(message proto.Message, filename string) error {
	data, err := ioutil.ReadFile(filename)
	if nil != err {
		return fmt.Errorf("cannot read binary data from file: %w", err)
	}

	if err := proto.Unmarshal(data, message); nil != err {
		return fmt.Errorf("cannot unmarshal binary to proto message: %w", err)
	}

	fmt.Println(message)

	return nil
}

// WriteProtobufToJSONFile write protocol buffer message to JSON file
func WriteProtobufToJSONFile(message proto.Message, fileName string) error {
	data, err := ProtobufToJSON(message)
	if err != nil {
		return fmt.Errorf("cannot marshal proto file to JSON: %w", err)
	}

	if err := ioutil.WriteFile(fileName, []byte(data), 0644); err != nil {
		return fmt.Errorf("cannot write JSON data to file: %w", err)
	}

	return nil
}
