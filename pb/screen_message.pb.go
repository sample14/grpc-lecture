// Code generated by protoc-gen-go. DO NOT EDIT.
// source: screen_message.proto

package pb

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Screen_Panel int32

const (
	Screen_UNKNOWN Screen_Panel = 0
	Screen_TN      Screen_Panel = 1
	Screen_IPS     Screen_Panel = 2
	Screen_OLED    Screen_Panel = 3
	Screen_AMOLED  Screen_Panel = 4
)

var Screen_Panel_name = map[int32]string{
	0: "UNKNOWN",
	1: "TN",
	2: "IPS",
	3: "OLED",
	4: "AMOLED",
}

var Screen_Panel_value = map[string]int32{
	"UNKNOWN": 0,
	"TN":      1,
	"IPS":     2,
	"OLED":    3,
	"AMOLED":  4,
}

func (x Screen_Panel) String() string {
	return proto.EnumName(Screen_Panel_name, int32(x))
}

func (Screen_Panel) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_8a2814cd02b8aba7, []int{0, 0}
}

type Screen struct {
	SizeInch             float32            `protobuf:"fixed32,1,opt,name=size_inch,json=sizeInch,proto3" json:"size_inch,omitempty"`
	Resolution           *Screen_Resolution `protobuf:"bytes,2,opt,name=resolution,proto3" json:"resolution,omitempty"`
	Panel                Screen_Panel       `protobuf:"varint,3,opt,name=panel,proto3,enum=grpc.lecture.Screen_Panel" json:"panel,omitempty"`
	Multitouch           bool               `protobuf:"varint,4,opt,name=multitouch,proto3" json:"multitouch,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *Screen) Reset()         { *m = Screen{} }
func (m *Screen) String() string { return proto.CompactTextString(m) }
func (*Screen) ProtoMessage()    {}
func (*Screen) Descriptor() ([]byte, []int) {
	return fileDescriptor_8a2814cd02b8aba7, []int{0}
}

func (m *Screen) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Screen.Unmarshal(m, b)
}
func (m *Screen) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Screen.Marshal(b, m, deterministic)
}
func (m *Screen) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Screen.Merge(m, src)
}
func (m *Screen) XXX_Size() int {
	return xxx_messageInfo_Screen.Size(m)
}
func (m *Screen) XXX_DiscardUnknown() {
	xxx_messageInfo_Screen.DiscardUnknown(m)
}

var xxx_messageInfo_Screen proto.InternalMessageInfo

func (m *Screen) GetSizeInch() float32 {
	if m != nil {
		return m.SizeInch
	}
	return 0
}

func (m *Screen) GetResolution() *Screen_Resolution {
	if m != nil {
		return m.Resolution
	}
	return nil
}

func (m *Screen) GetPanel() Screen_Panel {
	if m != nil {
		return m.Panel
	}
	return Screen_UNKNOWN
}

func (m *Screen) GetMultitouch() bool {
	if m != nil {
		return m.Multitouch
	}
	return false
}

type Screen_Resolution struct {
	Width                uint32   `protobuf:"varint,1,opt,name=width,proto3" json:"width,omitempty"`
	Height               uint32   `protobuf:"varint,2,opt,name=height,proto3" json:"height,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Screen_Resolution) Reset()         { *m = Screen_Resolution{} }
func (m *Screen_Resolution) String() string { return proto.CompactTextString(m) }
func (*Screen_Resolution) ProtoMessage()    {}
func (*Screen_Resolution) Descriptor() ([]byte, []int) {
	return fileDescriptor_8a2814cd02b8aba7, []int{0, 0}
}

func (m *Screen_Resolution) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Screen_Resolution.Unmarshal(m, b)
}
func (m *Screen_Resolution) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Screen_Resolution.Marshal(b, m, deterministic)
}
func (m *Screen_Resolution) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Screen_Resolution.Merge(m, src)
}
func (m *Screen_Resolution) XXX_Size() int {
	return xxx_messageInfo_Screen_Resolution.Size(m)
}
func (m *Screen_Resolution) XXX_DiscardUnknown() {
	xxx_messageInfo_Screen_Resolution.DiscardUnknown(m)
}

var xxx_messageInfo_Screen_Resolution proto.InternalMessageInfo

func (m *Screen_Resolution) GetWidth() uint32 {
	if m != nil {
		return m.Width
	}
	return 0
}

func (m *Screen_Resolution) GetHeight() uint32 {
	if m != nil {
		return m.Height
	}
	return 0
}

func init() {
	proto.RegisterEnum("grpc.lecture.Screen_Panel", Screen_Panel_name, Screen_Panel_value)
	proto.RegisterType((*Screen)(nil), "grpc.lecture.Screen")
	proto.RegisterType((*Screen_Resolution)(nil), "grpc.lecture.Screen.Resolution")
}

func init() {
	proto.RegisterFile("screen_message.proto", fileDescriptor_8a2814cd02b8aba7)
}

var fileDescriptor_8a2814cd02b8aba7 = []byte{
	// 275 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x90, 0xcd, 0x4a, 0xf3, 0x40,
	0x14, 0x86, 0xbf, 0x99, 0xfc, 0x34, 0xdf, 0xa9, 0x95, 0x70, 0x28, 0x12, 0x2a, 0x68, 0xe8, 0x2a,
	0xab, 0x20, 0x75, 0xa7, 0x0b, 0x51, 0x74, 0x51, 0xd4, 0xb4, 0x4c, 0x15, 0xc1, 0x4d, 0x69, 0xc7,
	0x43, 0x32, 0x90, 0x26, 0x21, 0x99, 0x20, 0x78, 0x57, 0xde, 0xa1, 0x74, 0x22, 0x9a, 0x85, 0xbb,
	0x39, 0x2f, 0xcf, 0xfb, 0xc3, 0xc0, 0xb8, 0x91, 0x35, 0x51, 0xb1, 0xde, 0x51, 0xd3, 0x6c, 0x52,
	0x8a, 0xab, 0xba, 0xd4, 0x25, 0x1e, 0xa4, 0x75, 0x25, 0xe3, 0x9c, 0xa4, 0x6e, 0x6b, 0x9a, 0x7e,
	0x72, 0x70, 0x57, 0x06, 0xc3, 0x63, 0xf8, 0xdf, 0xa8, 0x0f, 0x5a, 0xab, 0x42, 0x66, 0x01, 0x0b,
	0x59, 0xc4, 0x85, 0xb7, 0x17, 0xe6, 0x85, 0xcc, 0xf0, 0x0a, 0xa0, 0xa6, 0xa6, 0xcc, 0x5b, 0xad,
	0xca, 0x22, 0xe0, 0x21, 0x8b, 0x86, 0xb3, 0xd3, 0xb8, 0x1f, 0x15, 0x77, 0x31, 0xb1, 0xf8, 0xc1,
	0x44, 0xcf, 0x82, 0x67, 0xe0, 0x54, 0x9b, 0x82, 0xf2, 0xc0, 0x0a, 0x59, 0x74, 0x38, 0x9b, 0xfc,
	0xe9, 0x5d, 0xee, 0x09, 0xd1, 0x81, 0x78, 0x02, 0xb0, 0x6b, 0x73, 0xad, 0x74, 0xd9, 0xca, 0x2c,
	0xb0, 0x43, 0x16, 0x79, 0xa2, 0xa7, 0x4c, 0x2e, 0x00, 0x7e, 0xbb, 0x70, 0x0c, 0xce, 0xbb, 0x7a,
	0xd3, 0xdd, 0xf2, 0x91, 0xe8, 0x0e, 0x3c, 0x02, 0x37, 0x23, 0x95, 0x66, 0xda, 0x4c, 0x1e, 0x89,
	0xef, 0x6b, 0x7a, 0x09, 0x8e, 0xe9, 0xc2, 0x21, 0x0c, 0x9e, 0x93, 0xfb, 0x64, 0xf1, 0x92, 0xf8,
	0xff, 0xd0, 0x05, 0xfe, 0x94, 0xf8, 0x0c, 0x07, 0x60, 0xcd, 0x97, 0x2b, 0x9f, 0xa3, 0x07, 0xf6,
	0xe2, 0xe1, 0xee, 0xd6, 0xb7, 0x10, 0xc0, 0xbd, 0x7e, 0x34, 0x6f, 0xfb, 0xc6, 0x7e, 0xe5, 0xd5,
	0x76, 0xeb, 0x9a, 0xef, 0x3c, 0xff, 0x0a, 0x00, 0x00, 0xff, 0xff, 0x3b, 0x65, 0x38, 0xd6, 0x66,
	0x01, 0x00, 0x00,
}
