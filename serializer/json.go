package serializer

import (
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
)

// ProtobufToJSON convert protocol buffer message to JSON string
func ProtobufToJSON(message proto.Message) (json string, err error) {
	marshaller := jsonpb.Marshaler{
		EnumsAsInts:  false,
		EmitDefaults: true,
		Indent:       "    ",
		OrigName:     true,
		AnyResolver:  nil,
	}
	return marshaller.MarshalToString(message)
}
