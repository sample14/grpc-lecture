package serializer

import (
	"gRPC_Lecture/pb"
	"gRPC_Lecture/sample"
	"github.com/golang/protobuf/proto"
	"testing"
)

func TestWriteProtobufToBinaryFile(t *testing.T) {
	type args struct {
		message  proto.Message
		filename string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "1. Success",
			args: args{
				message:  sample.NewLaptop(),
				filename: "../tmp/laptop.bin",
			},
			wantErr: false,
		},
		{
			name: "2. Incorrect message input",
			args: args{
				message:  nil,
				filename: "../tmp/memory.bin",
			},
			wantErr: true,
		},
		{
			name: "3. File not found",
			args: args{
				message:  sample.NewLaptop(),
				filename: "../tmp1/laptop.bin",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := WriteProtobufToBinaryFile(tt.args.message, tt.args.filename); (err != nil) != tt.wantErr {
				t.Errorf("WriteProtobufToBinaryFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestReadProtobufFromBinaryFile(t *testing.T) {
	type args struct {
		message  proto.Message
		filename string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "1. Success",
			args: args{
				message:  &pb.Laptop{},
				filename: "../tmp/laptop.bin",
			},
			wantErr: false,
		},
		{
			name: "2. File not found",
			args: args{
				message:  &pb.Laptop{},
				filename: "../tmp/memories.bin",
			},
			wantErr: true,
		},
		{
			name: "3. Incorrect message output",
			args: args{
				message:  &pb.Screen{},
				filename: "../tmp/laptop.bin",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ReadProtobufFromBinaryFile(tt.args.message, tt.args.filename); (err != nil) != tt.wantErr {
				t.Errorf("ReadProtobufFromBinaryFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestReadAndWriteProtobufForBinaryFile(t *testing.T) {
	t.Parallel()

	binaryFile := "../tmp/laptop_bin.bin"

	laptop1 := sample.NewLaptop()
	t.Run("Write data to binary file", func(t *testing.T) {
		if err := WriteProtobufToBinaryFile(laptop1, binaryFile); err != nil {
			t.Errorf("Failed to write")
		}
	})

	laptop2 := &pb.Laptop{}
	t.Run("Read data from binary file", func(t *testing.T) {
		if err := ReadProtobufFromBinaryFile(laptop2, binaryFile); err != nil {
			t.Errorf("Failed to read")
		}
	})

	t.Run("Compare data from binary file with original data", func(t *testing.T) {
		if !proto.Equal(laptop1, laptop2) {
			t.Errorf("Data from binary file not the same with original one")
		}
	})
}

func TestWriteProtobufToJSONFile(t *testing.T) {
	fileName := "../tmp/laptop.json"
	type args struct {
		message  proto.Message
		fileName string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "1. Protobuf message is nil",
			args: args{
				message:  nil,
				fileName: fileName,
			},
			wantErr: true,
		},
		{
			name: "2. Cannot write file (File name incorrect)",
			args: args{
				message:  sample.NewLaptop(),
				fileName: "//",
			},
			wantErr: true,
		},
		{
			name: "3. Write JSON file successfully",
			args: args{
				message:  sample.NewLaptop(),
				fileName: fileName,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := WriteProtobufToJSONFile(tt.args.message, tt.args.fileName); (err != nil) != tt.wantErr {
				t.Errorf("WriteProtobufToJSONFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
