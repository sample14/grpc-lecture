FROM golang as builder
RUN mkdir /compile
ADD ./ /compile/
WORKDIR /compile
RUN go mod vendor
RUN CGO_ENABLED=0 go build -mod vendor -o gRPC_Lecture

############################################
FROM alpine
RUN mkdir /myapp
COPY --from=builder /compile/gRPC_Lecture /myapp/

ENTRYPOINT /myapp/gRPC_Lecture
EXPOSE 8080